require './pieces'
require 'debugger'
require 'colorize'

class GameBoard
  attr_accessor :board, :w_king, :b_king

  def initialize
    @board = Array.new(8) { Array.new(8, nil)}
    populate_pawns
    populate_back_row
  end

  def [](pos)
    x, y = pos[0], pos[1]
    self.board[x][y]
  end

  def []=(pos, mark)
    x, y = pos[0], pos[1]
    self.board[x][y] = mark
  end


  def populate_pawns
    pawn_rows = { 1 => "B", 6 => "W"}

    pawn_rows.each do |row, color|
      self.board[row].each_with_index do |row_el, col|
        self.board[row][col] = Pawn.new(color, [row, col], self)
      end
    end

  end

  def populate_back_row
    back_row = { 0 => "B", 7 => "W" }
    back_row.each do |row, color|

      self.board[row].each_with_index do |row_el, col|

        if    col == 0 || col == 7
          self.board[row][col] = Rook.new(color, [row, col], self)

        elsif col == 1 || col == 6
          self.board[row][col] = Knight.new(color, [row, col], self)

        elsif col == 2 || col == 5
          self.board[row][col] = Bishop.new(color, [row, col], self)

        elsif col == 3
          self.board[row][col] = Queen.new(color, [row, col], self)

        else
          self.board[row][col] = King.new(color, [row, col], self)

          self.b_king = self.board[row][col] if color == "B"
          self.w_king = self.board[row][col] if color == "W"

        end
      end
    end
  end

  def move(start_pos, end_pos, color)
    current_el = self[start_pos]

    #need to add more exceptions
    raise "Not your piece to move. *Back Hand*" if current_el.color != color
    raise "No piece in start position" if current_el.nil?
    raise "Invalid move" unless current_el.possible_moves.include?(end_pos)
    raise "Invalid move, you cannot put yourself in check" if move_into_check?(start_pos, end_pos)

    self[end_pos] = current_el
    current_el.pos = end_pos
    self[start_pos] = nil

  end

  def move_into_check?(start_pos, end_pos)
    test_board = self.dup_board
    color = test_board[start_pos].color
    test_board.dup_move(start_pos, end_pos)

    test_board.checked?(color)
  end

  #color must be a string: "W"
  def checked?(color)
    evaluated_king = self.w_king if color == "W"
    evaluated_king = self.b_king if color == "B"

    self.board.flatten.any? do |piece|
      piece && piece.color != color && piece.possible_moves.include?(evaluated_king.pos)
    end

    # self.board.each do |row|
    #
    #   row.each do |el|
    #     next if (el == nil) || (el.color == color)
    #     return true if el.possible_moves.include?(evaluated_king.pos)
    #   end
    #
    # end
    # return false
  end

  def dup_move(start_pos, end_pos)
    current_el = self[start_pos]
    self[end_pos] = current_el
    current_el.pos = end_pos
    self[start_pos] = nil
  end

  def dup_board
    duped_board = GameBoard.new

    self.board.each_with_index do |row, row_idx|
      row.each_with_index do |el, col_idx|
        if el.nil?
          duped_board[[row_idx, col_idx]] = nil
        else
          color, pos = el.color, el.pos
          duped_board[[row_idx, col_idx]] = el.class.new(color, pos, duped_board)
        end
      end
    end

    duped_board
  end

  def render
    print "   "
    ("A".."H").each {|col_ref| print " #{col_ref} "}
    print "\n"
    self.board.each_with_index do |row, row_i|
      print " #{8-row_i} "
      row.each_with_index do |el, col_i|
        colored_square = (row_i.even? && col_i.even?) || (row_i.odd? && col_i.odd?)
        el == nil ? color_piece(colored_square) : color_piece(colored_square, el)
      end
      print "\n"
    end
  end

  def print_square(str, background_color, piece_color = :red)
    print str.colorize(:color => piece_color, :background => background_color)
  end

  def color_piece(colored_square, piece = "   ")
    background = :white
    background = :light_black if colored_square
    spaced_value = " #{piece.value} " unless piece.is_a?(String)

    if piece.class != String
      piece.color == "W" ? print_square(spaced_value, background,:red) : print_square(spaced_value,background,:blue)
    else
      print_square(piece, background)
    end

  end

  def remove_checks(el)
    el.possible_moves.select do |end_pos|
      !self.move_into_check?(el.pos, end_pos)
    end
  end

  def checkmate?(color)
    # self.board.flatten.none?
    movable_pieces = 0
    self.board.each do |row|
      row.each do |el|
        next if el.nil?
        if el.color == color
          valid_moves = remove_checks(el)
          movable_pieces += 1 if valid_moves.count > 0
        end
      end
    end

    return true if movable_pieces == 0
    return false
  end

end


# #checkmate sequence:
b = GameBoard.new

b.move([6, 4], [4, 4], "W")
b.move([1, 4], [3, 4], "B")
b.move([7, 5], [4, 2], "W")
b.move([0, 5], [3, 2], "B")
b.move([7, 3], [4, 6], "W")
b.move([0, 1], [2, 2], "B")
b.move([4, 6], [1, 6], "W")
b.move([0, 3], [4, 7], "B")
b.render





