def list_all_moves(positional_array)
  all_moves = []
  positional_array.each do |dir|

    next_position = [ (0 + dir[0]) , (0 + dir[1]) ]
    next unless within_range?(next_position)

    # unless invalid_move?(next_position)
      all_moves << next_position
    # end
  end
  #this is an array with all possible moves for current piece
  all_moves
end

def within_range?(position)
  position.all? {|coord| coord.between?(0,7)}
end

# def invalid_move?(position)
#   square_el = self.board[position[0]][position[1]]
#
#   if square_el.nil? || (square_el.color != self.color)
#     return false
#   end
#
#   return true
# end

board = Array.new(8) { Array.new(8, nil)}
board[0][0] = "B"

p list_all_moves([[1,2],[1,-2],[-1,2],[-1,-2],[2,1],[2,-1],[-2,1],[-2,-1]])

