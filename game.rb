require './gameboard.rb'
require './pieces.rb'

class Game
  attr_reader :board, :player1, :player2

  def initialize
    @board = GameBoard.new
    @player1 = HumanPlayer.new
    @player2 = HumanPlayer.new
    start_game
  end

  def start_game

    until self.board.checkmate?("W") || self.board.checkmate?("B")

      self.board.render
      puts "Player 1's move"
      begin
        player1_move = self.player1.play_turn
        self.board.move(player1_move[0], player1_move[1], "W")
      rescue RuntimeError => e
        puts "Error was: #{e.message}"
        retry
      end

      self.board.render
      puts "Player 2's move"
      begin
        player2_move = self.player2.play_turn
        self.board.move(player2_move[0], player2_move[1], "B")
      rescue RuntimeError => e
        puts "Error was: #{e.message}"
        retry
      end

    end

    puts "GAME OVER"
  end

end

class HumanPlayer

  def play_turn
    #user passes in letternumber (F6)
    column_ref = {"A" => 0, "B" => 1, "C" => 2, "D" => 3, "E" => 4, "F" => 5, "G" => 6, "H" => 7}
    row_ref    = {"8" => 0, "7" => 1, "6" => 2, "5" => 3, "4" => 4, "3" => 5, "2" => 6, "1" => 7}

    puts "Which piece would you like to move?"
    start_input     =  gets.chomp
    start_input_arr =  start_input.split("").map.with_index do |el, idx|
      if idx == 0
        el.upcase
      else
        el
      end
    end
    start_indices   =  [ row_ref[start_input_arr[1]] , column_ref[start_input_arr[0]]]

    puts "Where do you want to move this piece to?"
    end_input       =  gets.chomp
    end_input_arr   =  end_input.split("").map.with_index do |el, idx|
      if idx == 0
        el.upcase
      else
        el
      end
    end
    end_indices     =  [ row_ref[end_input_arr[1]] , column_ref[end_input_arr[0]]]
    [start_indices, end_indices]
  end

end
















