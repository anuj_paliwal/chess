#Piece class
# => attr_reader :color, :pos(will need to get from board)

class Piece
  attr_accessor :pos, :possible_moves
  attr_reader :color, :board_obj, :value

  def initialize(color, pos, board)
    @color = color
    @pos = pos
    @board_obj = board
    @possible_moves = []
    @value = " "
  end

  def within_range?(position)
    position.all? { |coord| coord.between?(0,7) }
  end

  def invalid_move?(position)
    square_el = self.board_obj[position]

    # !(square_el.nil? || (square_el.color != self.color))

    square_el && (square_el.color == self.color)
  end

end

class Sliding < Piece
  PERPENDICULAR = [[1, 0], [0, 1], [-1, 0], [0, -1]]
  DIAGONALS = [[1, 1], [1, -1], [-1, 1], [-1, -1]]


  def possible_move?(position)
     !within_range?(position) || invalid_move?(position)
  end

  def list_all_moves(directional_array)
    all_moves = []

    directional_array.each do |dir|
      hit_object = false
      next_position = [ (self.pos[0] + dir[0]) , (self.pos[1] + dir[1]) ]

      until hit_object == true || possible_move?(next_position)
        all_moves << next_position

        hit_object = true if self.board_obj[next_position]

        next_position = [next_position[0] + dir[0], next_position[1] + dir[1]]
      end
    end

    all_moves
  end

end

class Rook < Sliding

  def initialize(color, pos, board)
    super(color, pos, board)
    @value = "\u265C".encode('utf-8')
  end

  def possible_moves
    self.possible_moves = list_all_moves(PERPENDICULAR)
  end

end

class Bishop < Sliding

  def initialize(color, pos, board)
    super(color, pos, board)
    @value = "\u265D".encode('utf-8')
  end

  def possible_moves
    self.possible_moves = list_all_moves(DIAGONALS)

  end

end

class Queen < Sliding

  def initialize(color, pos, board)
    super(color, pos, board)
    @value = "\u265B".encode('utf-8')
  end

  def possible_moves
    self.possible_moves = list_all_moves(PERPENDICULAR+DIAGONALS)

  end

end

class Stepping < Piece

  def list_all_moves(positional_array)
    all_moves = []
    positional_array.each do |dir|

      next_position = [ (self.pos[0] + dir[0]) , (self.pos[1] + dir[1]) ]
      next unless within_range?(next_position)

      unless invalid_move?(next_position)
        all_moves << next_position
      end
    end

    all_moves
  end

end

class Knight < Stepping
  KNIGHT_POSITIONALS = [[1,2],[1,-2],[-1,2],[-1,-2],[2,1],[2,-1],[-2,1],[-2,-1]]

  def initialize(color, pos, board)
    super(color, pos, board)
    @value = "\u265E".encode('utf-8')
  end

  def possible_moves
    self.possible_moves = list_all_moves(KNIGHT_POSITIONALS)
  end

end

class King < Stepping
  KING_POSITIONALS = [ [1, 0],[-1, 0],[0, 1],[0, -1],[1, 1],
                    [1, -1], [-1, -1], [-1, 1] ]

  def initialize(color, pos, board)
    super(color, pos, board)
    @value = "\u265A".encode('utf-8')
  end

  def possible_moves
    unfiltered_moves = list_all_moves(KING_POSITIONALS)
  end

end

class Pawn < Piece
  attr_accessor :first_move

  def initialize(color, pos, board)
    super(color, pos, board)
    @first_move = true
    @value = "\u265F".encode('utf-8')
  end

  def first_move?
    self.first_move
  end

  def list_all_moves
    all_moves = []
    all_moves += list_front_three
    all_moves << two_spaces_ahead if self.first_move? && !two_spaces_ahead.empty?
    all_moves
  end

  def two_spaces_ahead
    front_dir = [[-1, 0], [-2, 0]] if self.color == "W"
    front_dir = [[1, 0], [2, 0]] if self.color == "B"

    one_ahead = [self.pos[0] + front_dir[0][0], self.pos[1] + front_dir[0][1]]
    two_ahead = [self.pos[0] + front_dir[1][0], self.pos[1] + front_dir[1][1]]

    one_ahead_empty = self.board_obj[one_ahead].nil?
    two_ahead_empty = self.board_obj[two_ahead].nil?
    if one_ahead_empty && two_ahead_empty
      [self.pos[0] + front_dir[1][0], self.pos[1] + front_dir[1][1]]
    else
      []
    end
  end

  def list_front_three
    positional_array = [[-1,0], [-1,1], [-1,-1]] if self.color == "W"
    positional_array = [[1,0], [1,1], [1,-1]] if self.color == "B"

    all_moves = []
    positional_array.each_with_index do |dir,index|
      next_position = [ (self.pos[0] + dir[0]) , (self.pos[1] + dir[1]) ]
      next unless within_range?(next_position)

      if index == 0
        all_moves << next_position if self.board_obj.board[next_position[0]][next_position[1]].nil?
      elsif !self.board_obj.board[next_position[0]][next_position[1]].nil?
        all_moves << next_position unless self.board_obj.board[next_position[0]][next_position[1]].color == self.color
      end


    end
    all_moves
  end

  def possible_moves
    self.possible_moves = list_all_moves
  end

end

